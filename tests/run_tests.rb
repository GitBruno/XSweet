#!/usr/bin/env ruby

require 'fileutils'

puts "Unzipping test documents..."

scriptPath = File.dirname( __FILE__ )
convertDir = "#{scriptPath}" + "/test-docs"

test_list = Dir["#{convertDir}/*"].select {|cand| File.directory? cand}

test_list.each do |book|

  docx_list = Dir["#{book}/*.docx"]

  docx_list.each do |docx|
    no_space =  docx.split(" ").join
    if docx != no_space
      FileUtils.mv(docx, no_space)
    end

    zip_name_no_ext = no_space.slice(0...-5)
    zip_name_ext = zip_name_no_ext + ".zip"

    FileUtils.cp(no_space, zip_name_ext)

    # %x`open #{zip_name_ext}`

    %x`unzip -o #{zip_name_ext} -d #{zip_name_no_ext}`

  end

  sleep(8)

  zip_list = Dir["#{book}/*.zip"]
  zip_list.each do |zip|
    FileUtils.rm zip
  end

end

puts "Converting test documents..."

test_list = Dir["#{convertDir}/*"].select {|cand| File.directory? cand}
test_list.delete("#{convertDir}/temp")

test_list.each do |book|
  book = book.split("/").last
  puts "Test: #{book}"

  file_path_list = Dir["#{convertDir}/#{book}/*"].select {|cand| File.directory? cand}
  # puts file_path_list

  file_list = []
  file_path_list.each do |file_path|
    file_list << file_path.split("/").last
  end

  file_list.each do |chapter|
    puts "converting: #{chapter}"
    %x`sh #{scriptPath}/tools/execute_chain.sh #{convertDir}/#{book} #{book} #{chapter}`
  end

  puts "done with #{book}"
end

# I am using exec as I want to keep the colouring
# Check for changes, expand paths as exec will spawn a new process from home
expectedPath = File.expand_path( "#{scriptPath}" + "/expected_outputs")
resultPath = File.expand_path( "#{scriptPath}" + "/outputs")
exec("git diff --no-index #{expectedPath} #{resultPath}")
