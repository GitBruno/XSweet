#!/bin/bash
# Testing HTML5 outputs via XSweet XSLT from sources extracted from .docx (Office Open XML)

P=$1
BOOKNAME=$2
DOCNAME=$3

#current directory
CURDIR="${0%/*}"

# path to xsweet
P_XSWEET="${CURDIR}/../.."
# path to output
P_OUT="${P_XSWEET}/tests/outputs"

# Note Saxon is included with this distribution, qv for license.
saxonHE="java -jar ${P_XSWEET}/lib/SaxonHE9-8-0-1J/saxon9he.jar"  # SaxonHE (XSLT 2.0 processor)


################
# applications #
################

# docx-extract
EXTRACT="${P_XSWEET}/applications/docx-extract/docx-html-extract.xsl"                     # "Extraction" stylesheet
  # NOTE: RUNS TABLE EXTRACTION FROM INSIDE EXTRACT
NOTES="${P_XSWEET}/applications/docx-extract/handle-notes.xsl"                            # "Refinement" stylesheets
SCRUB="${P_XSWEET}/applications/docx-extract/scrub.xsl"
JOIN="${P_XSWEET}/applications/docx-extract/join-elements.xsl"
COLLAPSEPARA="${P_XSWEET}/applications/docx-extract/collapse-paragraphs.xsl"

# list-promote
PROMOTELISTS="${P_XSWEET}/applications/list-promote/PROMOTE-lists.xsl"
  # NOTE: RUNS mark-lists, then itemize-lists

# html-polish
FINALRINSE="${P_XSWEET}/applications/html-polish/final-rinse.xsl"
XMLTOHTML5="${P_XSWEET}/applications/html-polish/html5-serialize.xsl"

# Intermediate and final outputs (serializations) are all left on the file system.

###########
# convert #
###########

# docx-extract

$saxonHE -xsl:$EXTRACT -s:$P/$DOCNAME/word/document.xml -o:$P_OUT/$BOOKNAME/$DOCNAME-1EXTRACTED.xhtml
echo Made $DOCNAME-1EXTRACTED.xhtml

$saxonHE -xsl:$NOTES -s:$P_OUT/$BOOKNAME/$DOCNAME-1EXTRACTED.xhtml -o:$P_OUT/$BOOKNAME/$DOCNAME-2NOTES.xhtml
echo Made $DOCNAME-2NOTES.xhtml

$saxonHE -xsl:$SCRUB -s:$P_OUT/$BOOKNAME/$DOCNAME-2NOTES.xhtml -o:$P_OUT/$BOOKNAME/$DOCNAME-3SCRUBBED.xhtml
echo Made $DOCNAME-3SCRUBBED.xhtml

$saxonHE -xsl:$JOIN -s:$P_OUT/$BOOKNAME/$DOCNAME-3SCRUBBED.xhtml -o:$P_OUT/$BOOKNAME/$DOCNAME-4JOINED.xhtml
echo Made $DOCNAME-4JOINED.xhtml

$saxonHE -xsl:$COLLAPSEPARA -s:$P_OUT/$BOOKNAME/$DOCNAME-4JOINED.xhtml -o:$P_OUT/$BOOKNAME/$DOCNAME-5COLLAPSED.xhtml
echo Made $DOCNAME-5COLLAPSED.xhtml

# list-promote

$saxonHE -xsl:$PROMOTELISTS -s:$P_OUT/$BOOKNAME/$DOCNAME-5COLLAPSED.xhtml -o:$P_OUT/$BOOKNAME/$DOCNAME-6PROMOTELISTS.xhtml
echo Made $DOCNAME-6PROMOTELISTS.xhtml

# html-polish

$saxonHE -xsl:$FINALRINSE -s:$P_OUT/$BOOKNAME/$DOCNAME-6PROMOTELISTS.xhtml -o:$P_OUT/$BOOKNAME/$DOCNAME-7RINSED.xhtml
echo Made $DOCNAME-7RINSED.xhtml

$saxonHE -xsl:$XMLTOHTML5 -s:$P_OUT/$BOOKNAME/$DOCNAME-7RINSED.xhtml -o:$P_OUT/$BOOKNAME/$DOCNAME-8HTML5.html
echo Made $DOCNAME-8HTML5.html
